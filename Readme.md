# Go Project generator

## Requirements

* git
* ansible
* ansible-playbook

## Generate API

```
❯ ansible-playbook generate-api.yaml
Gitlab repo url: git@bitbucket.org:DmitriyTelepnev/simple-payment-system.git
Common path to projects: /tmp


PLAY [Generate API GO project] *********************************************************************************************************************************************

TASK [Gathering Facts] *****************************************************************************************************************************************************
ok: [localhost]

.......
```

Где `Gitlab repo url` - URL проекта, для которого вы хотите сгенерить шаблон
`Common path to projects` - путь, где лежат у вас Go-шные проекты

Сгенерированный проект будет находиться по пути `<Common path to projects>/<Gitlab repo URL>`

